﻿using System.Web.UI;
using System.Web.UI.WebControls;

/// <summary>
/// Code behind file for the products page.
/// </summary>
/// <author>
/// Destiny Harris
/// </author>
/// <version>
/// March 10, 2015 | Spring
/// </version>
public partial class Products : Page
{

    /// <summary>
    /// Handles the RowUpdated event of the gvProducts control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="GridViewUpdatedEventArgs"/> instance containing the event data.</param>
    protected void gvProducts_RowUpdated(object sender, GridViewUpdatedEventArgs e)
    {
        if (e.Exception != null)
        {
            this.lblErrorMessage.Text = "A databse error has occurred." + "Message: " + e.Exception.Message;
            e.ExceptionHandled = true;
            e.KeepInEditMode = true;
        }
        else
        {
            this.lblErrorMessage.Text = "";
        }
    }
}