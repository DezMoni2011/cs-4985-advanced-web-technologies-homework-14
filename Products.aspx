﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Products.aspx.cs" Inherits="Products" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Chapter 14: Northwind</title>
    <link href="Main.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <header>
        &nbsp;<img alt="Northwind Solutions" src="Images/Northwind.jpg" /><br />
    </header>
    <section>
    <form id="form1" runat="server">
        &nbsp;<br />
        
        <asp:GridView ID="gvProducts" runat="server" AutoGenerateColumns="False" DataKeyNames="ProductID" DataSourceID="sdsProducts" BackColor="White" BorderColor="#E7E7FF" BorderStyle="None" BorderWidth="1px" CellPadding="3" GridLines="Horizontal" AllowSorting="True" AllowPaging="True" PageSize="15" OnRowUpdated="gvProducts_RowUpdated">
            <AlternatingRowStyle BackColor="#F7F7F7"></AlternatingRowStyle>
            <Columns>
                <asp:BoundField DataField="ProductID" HeaderText="ID" ReadOnly="True" InsertVisible="False" SortExpression="ProductID">
                    <HeaderStyle HorizontalAlign="Center" Width="50px"></HeaderStyle>

                    <ItemStyle HorizontalAlign="Center" Width="50px"></ItemStyle>
                </asp:BoundField>
                <asp:BoundField DataField="ProductName" HeaderText="Name" SortExpression="ProductName">
                    <ItemStyle Width="250px"></ItemStyle>
                </asp:BoundField>
                <asp:TemplateField HeaderText="Units On Hand" SortExpression="UnitsInStock">
                    <EditItemTemplate>
                        <asp:TextBox ID="txtUnitsOnHand" runat="server" Text='<%# Bind("UnitsInStock") %>'></asp:TextBox>
                        <asp:RequiredFieldValidator ID="rfvUnitsOnHand" runat="server" ControlToValidate="txtUnitsOnHand" CssClass="error" Display="Dynamic" ErrorMessage="This field is required.">*</asp:RequiredFieldValidator>
                        <asp:CompareValidator ID="cvUnitsOnHand" runat="server" ControlToValidate="txtUnitsOnHand" CssClass="error" Display="Dynamic" ErrorMessage="Must be positive number" Operator="GreaterThanEqual" Type="Integer" ValueToCompare="0">*</asp:CompareValidator>
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label1" runat="server" Text='<%# Bind("UnitsInStock") %>'></asp:Label>
                    </ItemTemplate>
                    <HeaderStyle HorizontalAlign="Center" Width="100px" />
                    <ItemStyle HorizontalAlign="Center" Width="100px" />
                </asp:TemplateField>
                <asp:CommandField ShowEditButton="True"></asp:CommandField>
            </Columns>
            <FooterStyle BackColor="#B5C7DE" ForeColor="#4A3C8C"></FooterStyle>

            <HeaderStyle BackColor="#4A3C8C" Font-Bold="True" ForeColor="#F7F7F7"></HeaderStyle>

            <PagerStyle HorizontalAlign="Right" BackColor="#E7E7FF" ForeColor="#4A3C8C"></PagerStyle>

            <RowStyle BackColor="#E7E7FF" ForeColor="#4A3C8C"></RowStyle>

            <SelectedRowStyle BackColor="#738A9C" Font-Bold="True" ForeColor="#F7F7F7"></SelectedRowStyle>

            <SortedAscendingCellStyle BackColor="#F4F4FD"></SortedAscendingCellStyle>

            <SortedAscendingHeaderStyle BackColor="#5A4C9D"></SortedAscendingHeaderStyle>

            <SortedDescendingCellStyle BackColor="#D8D8F0"></SortedDescendingCellStyle>

            <SortedDescendingHeaderStyle BackColor="#3E3277"></SortedDescendingHeaderStyle>
        </asp:GridView>
        <asp:SqlDataSource ID="sdsProducts" runat="server" ConnectionString='<%$ ConnectionStrings:NorthwindConnectionString %>' ProviderName='<%$ ConnectionStrings:NorthwindConnectionString.ProviderName %>' SelectCommand="SELECT [ProductID], [ProductName], [UnitsInStock] FROM [tblProducts]" UpdateCommand="UPDATE[tblProducts] SET[ProductName] = ?, [UnitsInStock] = ? WHERE[ProductID] = ?"></asp:SqlDataSource>
    
        <asp:Label ID="lblErrorMessage" runat="server" CssClass="error" Text=""></asp:Label>
        <br />
        <br />
        <asp:ValidationSummary ID="vsProducts" runat="server" CssClass="error" />
    </form>
    </section>
</body>
</html>
